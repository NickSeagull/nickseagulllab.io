# Mkdocs - GitBook Theme

[![Apache 2.0 License][apache-badge]](LICENSE)

[apache-badge]: http://img.shields.io/badge/license-apache-blue.svg

<a href="https://gitlab.com/lramage94/mkdocs-gitbook-theme"><img src="img/screenshot.png" alt="Default theme for GitBook for Mkdocs"></a>